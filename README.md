#Критерии для разработчиков JS

##Ступень II

* Настройка рабочего окружения (Windows, Linux, MacOS)
* npm, yarn
* IDE
    * WebStorm (например)
    * Chrome Dev Tools

### Теория
* Типы данных и встроенные методы работы с ними: arrays, objects, strings, numbers
* Замыкания
* Области видимости
* Фреймворки: 
    * jQuery
    * Сравнение с чистым JS (VanillaJS)
* Хранение данных
    * Local Storage
    * REST (JSON, JSONP, CORS)

##Ступень III

* ES6
* Сборка проектов
    * Webpack
    * RequireJS, AMD
* ReactJS + Redux
* AngularJS (или)
* Тестирование
    * TDD (qUnit, Mocha)
    * BDD (frontend) (CodeceptJS)

## Ступень IV

* Серверный JS
    * NodeJS
    * ExpressJS
    * MongoDB
    * MySQL
* Тестирование
    * BDD (backend) (CodeceptJS)
* Хранение данных на клиенте: 
    * IndexedDB
    * WebSQL (или)
* React Native